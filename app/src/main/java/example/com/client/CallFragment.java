package example.com.client;

import android.content.Context;
import android.graphics.Point;
import android.hardware.Camera;
import android.hardware.camera2.CameraManager;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.alicecallsbob.fcsdk.android.phone.Call;
import com.alicecallsbob.fcsdk.android.phone.CallCreationWithErrorException;
import com.alicecallsbob.fcsdk.android.phone.CallListener;
import com.alicecallsbob.fcsdk.android.phone.CallStatus;
import com.alicecallsbob.fcsdk.android.phone.CallStatusInfo;
import com.alicecallsbob.fcsdk.android.phone.Phone;
import com.alicecallsbob.fcsdk.android.phone.VideoSurface;
import com.alicecallsbob.fcsdk.android.phone.VideoSurfaceListener;

/**
 * Created by Nathan on 07/04/2016.
 */
public class CallFragment extends Fragment implements VideoSurfaceListener {

    private VideoSurface remoteSurface = null;
    private VideoSurface localSurface = null;

    private CheckBox audioCheckBox = null;
    private CheckBox videoCheckBox = null;
    private CheckBox rearCamCheckBox = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_call, container, false);

        // make the log scrollable & clear any lorem ipsum text
        TextView logView = (TextView) view.findViewById(R.id.log);
        logView.setMovementMethod(new ScrollingMovementMethod());
        logView.setText("");

        // wire up the dial / hangup
        Button dialButton = (Button) view.findViewById(R.id.dial);
        dialButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View button) {
                // hide the keyboard
                hideKeyboard();

                // find the address the user dialled
                EditText addressField = (EditText) view.findViewById(R.id.address);
                String address = addressField.getText().toString();

                // ensure that the video views are set up
                setupVideoViews(view);

                try {
                    // create the call
                    CallListener callListener = null;
                    boolean audio = audioCheckBox.isChecked();
                    boolean video = videoCheckBox.isChecked();
                    Call call = ConfigFragment.UC.getPhone().createCall(address, audio, video, callListener);

                    // determine where the remote video stream should be displayed
                    call.setVideoView(remoteSurface);
                } catch (Exception e) {
                    e.printStackTrace();
                    addMessageToLog("Error: " + e.getMessage());
                }

            }
        });

        Button hangupButton = (Button) view.findViewById(R.id.hangup);
        hangupButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View button) {
                // hide the keyboard
                hideKeyboard();

                // hangup a call (if we're on one)
                addMessageToLog("User clicked to hangup");
            }
        });


        // wire up the local audio / video & camera change check boxes
        this.audioCheckBox = (CheckBox)view.findViewById(R.id.audio);
        this.videoCheckBox = (CheckBox)view.findViewById(R.id.video);
        this.rearCamCheckBox = (CheckBox)view.findViewById(R.id.rear_cam);

        audioCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean checked) {
                addMessageToLog("Enable audio: " + checked);
            }
        });

        videoCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean checked) {
                addMessageToLog("Enable video: " + checked);
            }
        });

        rearCamCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton button, boolean checked) {
                addMessageToLog("Use rear cam: " + checked);
            }
        });

        // return the prepared statement
        return view;
    }

    // adds a message to the log
    private void addMessageToLog(final String message) {
        final TextView textView = (TextView) this.getView().findViewById(R.id.log);
        this.getActivity().runOnUiThread(new Runnable() {

            @Override
            public void run() {
                textView.setText(message + "\n" + textView.getText());
            }
        });
    }

    private void hideKeyboard() {
        InputMethodManager imm = (InputMethodManager)getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(this.getView().getWindowToken(), 0);
    }



    //////////////////////////////////////////
    //
    // Ensures that the video views are set up

    private void setupVideoViews(View view) {

        int width = 640;
        int height = 480;
        Point remoteDimensions = new Point(width, height);
        Point localDimensions = new Point(width / 4, height / 4);

        // construct the VideoSurface's
        Phone phone = ConfigFragment.UC.getPhone();
        this.remoteSurface = phone.createVideoSurface(this.getActivity(), remoteDimensions, this);
        this.localSurface = phone.createVideoSurface(this.getActivity(), localDimensions, this);

        // get references to the local / remote video surface containers in the UI
        RelativeLayout remoteContainer = (RelativeLayout) view.findViewById(R.id.remote);
        RelativeLayout localContainer = (RelativeLayout) view.findViewById(R.id.local);

        // empty the views
        remoteContainer.removeAllViews();
        localContainer.removeAllViews();

        // add the VideoSurface's to the UI
        remoteContainer.addView(remoteSurface);
        localContainer.addView(localSurface);

        // now set the preview view on the phone
        phone.setPreviewView(localSurface);
        phone.setCamera(Camera.CameraInfo.CAMERA_FACING_FRONT);
    }




    //////////////////////////////////////////
    //
    // VideoSurfaceListener methods

    @Override
    public void onFrameSizeChanged(int width, int height, VideoSurface.Endpoint endpoint, VideoSurface videoSurface) {
        this.addMessageToLog("onFrameSizeChanged: " + width + ", " + height + " : " + endpoint);
    }

    @Override
    public void onSurfaceRenderingStarted(VideoSurface videoSurface) {
        this.addMessageToLog("onSurfaceRenderingStarted");
    }
}
